let firstNumber;
let mathOperation;
let secondNumber;

do {
  firstNumber = +prompt("Введите первое число");
} while (!isFinite(firstNumber) || firstNumber < 0);

do {
  mathOperation = prompt("Введите операцию (+, -, *, /)");
} while (!["+","-","*","/"].includes(mathOperation));

do {
  secondNumber = +prompt("Введите второе число");
} while (!isFinite(secondNumber) || secondNumber < 0);

function calculateResult(a, c, b) {
  switch (c) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
    default:
      return "Ошибка";
  }
}

const result = calculateResult(firstNumber, mathOperation, secondNumber);
console.log(result);